# 이메일 서명 적용방법 

Mac에서 기본 제공하는 Mail앱에서 할 수 있는 방법입니다.



## 1. 먼저 Mail앱을 연후 새메일을 열어 서명을 추가한 후 Mail앱을 완전히 종료시킵니다.





##2. terminal이나 해당 경로로 이동 

#### 2-1. 해당 경로로 들어가 TextEditor로 해당 경로의 파일을 엽니다.

```terminal
open -a TextEdit ~/Library/Mail/V5/MailData/Signatures/*.mailsignature
```

여기서 V5는 버전별로 다른듯 합니다. 체크해볼것.

#### 2-2. 복사했던 코드를 덮어씌우고 저장후 닫습니다.

#### 2-3. 해당 경로의 파일들을 lock시킵니다.

```terminal
chflags uchg ~/Library/Mail/V5/MailData/Signatures/*.mailsignature
```



## 3. 다시 Mail앱을 열어 서명을 확인하면 추가 됩니다.





### 참고

- <http://matt.coneybeare.me/how-to-make-an-html-signature-in-apple-mail-for-high-sierra-os-x-10-dot-13/>
- <https://macinjune.com/mac/tip/%EB%A7%A5-mail-%EC%95%B1%EC%97%90-html-%EC%84%9C%EB%AA%85-%EC%B6%94%EA%B0%80%ED%95%98%EA%B8%B0/>