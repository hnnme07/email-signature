;(function(global){

    let _this = global.document;
        
    _this.querySelector('#confirm').addEventListener('click', renderEmailSign,false);
    _this.querySelector('#confirm').addEventListener('keypress', renderEmailSign,false);

    function renderEmailSign(e){

        let name        = _this.querySelector('#name').value,
            englishName = _this.querySelector('#englishName').value,
            
            mobile      = _this.querySelector('#mobile').value,
            email       = _this.querySelector('#email').value,

            companyAddress = '서울특별시 강남구 강남대로 136길 11, 3층',
            companyPhoneNumber = '02-6013-6001',
            logoImage = 'http://www.voiceloco.com/wp-content/uploads/2018/02/vms_logo01.png',
            siteAddress = 'http://www.voiceloco.com';

            console.log('mobile', mobile)
            // position    = _this.querySelector('#position').value,
            // department  = _this.querySelector('#department').value,

      
        let memberData = getImage(name);
        let imageSource = memberData.memberImage,
            position = memberData.memberPosition,
            department = memberData.memberDepartment;

        let result = `
            <div>
                <table width="360px" height="150px"  cellpadding="0" cellspacing="0" border="0" style="letter-spacing:0.4px;padding:0;margin:0;border-collapse:collapse;font-family:나눔고딕,NanumGothic,돋움,Dotum,'Malgun Gothic','Lucida Grande',sans-serif;color:#687079;">
                    <col>
                    <colgroup span="2"></colgroup>
                    <colgroup span="2"></colgroup>
                    <tr style="height:22px;">
                        <td rowspan="3" width="60px" style="padding:0;">
                            <img src="${imageSource}" alt="${name}">
                        </td>
                        <td  scope="col" style="font-weight:bold;font-size:15px;line-height:16px;padding:0;padding-bottom:0;">
                            ${name} 
                            <span style="font-weight:bold;font-size:12px;padding-left:4px;">
                                ${position}<span style="padding:0 4px;font-weight:100">/</span>${department} 
                            </span>
                        </td>
                    </tr>
                    <tr style="height:0.5px;">
                        <td scope="col" style="color:#999999;font-size:11px;padding:0 0 6px 0;font-family:Arial, Helvetica, sans-serif;font-weight:100;">
                            ${englishName}
                        </td>
                    </tr>
                    <tr style="height:2px;">
                        <td scope="col" colspan="2" style="border-top:1px solid #eeeeee;padding-bottom:8px;"></td>
                    </tr>
                    <tr>
                        <td rowspan="4" width="90px" style="padding:0"></td>
                        <td scope="col" colspan="2" style="font-size:11px;font-weight:bold;line-height:8px;color:#687079;text-decoration:none!important;">
                            ${companyAddress}
                        </td>
                    </tr>
                    <tr>
                        <td scope="col" style="font-size:11px;font-weight:bold;font-style:normal;line-height:8px;text-decoration:none!important;color:#687079;">
                            <a href="tel:${companyPhoneNumber}" style="text-decoration:none;color:#687079;"> <span style="color:#ff675d;padding: 0 6px 0 0;">T.</span>${companyPhoneNumber}</a>
                            <a href="tel:${mobile}"  style="text-decoration:none;color:#687079"><span style="color:#ff675d;padding: 0 6px 0 8px;">M.</span>${mobile}</a>
                        </td>
                    </tr>
                    <tr>
                        <td scope="col" colspan="2" style="font-size:11px;font-weight:bold;line-height:6px;font-style:normal;text-decoration:none!important;">
                            <span style="color:#ff675d">E.</span>
                            <a href="mailto:${email}@voiceloco.com" style="color:#687079;text-decoration:none">${email}@voiceloco.com</a>
                        </td>
                    </tr>
                    <tr>
                        <td scope="col" colspan="2" style="padding-top:8px;">
                            <a href="${siteAddress}" target="_blank">
                                <img src="${logoImage}" alt="voiceloco">
                            </a>
                        </td>
                    </tr>
                </table>
            </div>`;

            _this.querySelector('#result').value = result;
            _this.querySelector('#resultImage').innerHTML = result;
    
    
        }



    function getImage(name){

        let memberList = [
            {name: '서준혁', image: 'http://www.voiceloco.com/wp-content/uploads/2018/02/vms_profile01.png', position: '대표', department: 'CEO'},

            {name: '이소현', image: 'http://www.voiceloco.com/wp-content/uploads/2018/02/vms_profile02_1.png', position: '책임', department: '사업팀'},
            {name: '이채린', image: 'http://www.voiceloco.com/wp-content/uploads/2018/02/vms_profile02_2.png', position: '매니저', department: '사업팀(기획)'},
            {name: '이유미', image: 'http://www.voiceloco.com/wp-content/uploads/2018/02/vms_profile02_3.png' ,position: '매니저', department: '사업팁(QA)'},
            {name: '서봉기', image: 'http://www.voiceloco.com/wp-content/uploads/2018/02/vms_profile02_4.png' ,position: '매니저', department: '사업팀(디자인)'},

            {name: '손두진', image: 'http://www.voiceloco.com/wp-content/uploads/2018/02/vms_profile03_1.png' ,position: '책임', department: '인프라운영'},
            {name: '이은강', image: 'http://www.voiceloco.com/wp-content/uploads/2018/02/vms_profile03_2.png' ,position: '연구원', department: '인프라운영'},

            {name: '이경순', image: 'http://www.voiceloco.com/wp-content/uploads/2018/02/vms_profile04_1.png' ,position: '책임', department: '개발팀(IMS/Proxy)'},
            {name: '이동규', image: 'http://www.voiceloco.com/wp-content/uploads/2018/02/vms_profile04_2.png' ,position: '연구원', department: '개발팀(Media server)'},
            {name: '오혁',  image: 'http://www.voiceloco.com/wp-content/uploads/2018/02/vms_profile04_3.png' ,position: '연구원', department: '개발팀(Media server)'},

            {name: '박세윤', image: 'http://www.voiceloco.com/wp-content/uploads/2018/02/vms_profile05_1.png' ,position: '책임', department: '개발팀(Rest/DB)'},
            {name: '문한솔', image: 'http://www.voiceloco.com/wp-content/uploads/2018/02/vms_profile05_2.png' ,position: '연구원', department: '개발팀(Admin/DB)'},
            {name: '김승현', image: 'http://www.voiceloco.com/wp-content/uploads/2018/02/vms_profile05_3.png' ,position: '연구원', department: '개발팀(Front-End)'},
            {name: '이지현', image: 'http://www.voiceloco.com/wp-content/uploads/2018/02/vms_profile05_4.png' ,position: '연구원', department: '개발팀(Front-End)'},

            {name: '박세훈', image: 'http://www.voiceloco.com/wp-content/uploads/2018/02/vms_profile06_1.png' ,position: '연구원', department: '개발팀(iOS)'},
            {name: '김상선', image: 'http://www.voiceloco.com/wp-content/uploads/2018/02/vms_profile06_2.png' ,position: '연구원', department: '개발팀(iOS)'},

            {name: '홍영택', image: 'http://www.voiceloco.com/wp-content/uploads/2018/02/vms_profile07_1.png' ,position: '연구원', department: '개발팀(Android)'},
            {name: '장현수', image: 'http://www.voiceloco.com/wp-content/uploads/2018/02/vms_profile07_2.png' ,position: '연구원', department: '개발팀(Android)'},

        ];

        let memberImage = '',
            memberPosition = '',
            memberDepartment = '';

        memberList.map(item =>{
            let memberName = item.name;

            if( memberName === name ){
                memberImage = item.image;
                memberPosition = item.position;
                memberDepartment = item.department;
            }

        });
        return {memberImage, memberPosition, memberDepartment};
    }

    

})(window);